package io.gitlab.chkypros.weeklyproject.week01;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class InputParser {
    @Value("${cupcakes.basic.cost}")
    private int BASIC_CUPCCAKE_COST;

    @Value("${cupcakes.deluxe.cost}")
    private int DELUXE_CUPCCAKE_COST;

    public Map<LocalDate, ADaysWork> parse(String basicsFilePath, String deluxesFilePath, String totalsFilePath) throws IOException {
        return parse(Paths.get(basicsFilePath), Paths.get(deluxesFilePath), Paths.get(totalsFilePath));
    }

    public Map<LocalDate, ADaysWork> parse(Path basicsFilePath, Path deluxesFilePath, Path totalsFilePath) throws IOException {
        Map<LocalDate, ADaysWork> data = new HashMap<>();

        // Ignore first (header) line from all inputs
        List<Integer> basics = Files.lines(basicsFilePath).skip(1).map(Integer::parseInt).collect(Collectors.toList());
        List<Integer> deluxes = Files.lines(deluxesFilePath).skip(1).map(Integer::parseInt).collect(Collectors.toList());
        List<Integer> totals = Files.lines(totalsFilePath).skip(1).map(Integer::parseInt).collect(Collectors.toList());

        if (basics.size() != deluxes.size() || basics.size() != totals.size()) {
            throw new IllegalArgumentException("Input files don't have equal number of entries");
        }

        LocalDate date = LocalDate.now();
        for (int i = basics.size() - 1; i >= 0; i--, date = date.minusDays(1)) {
            Integer basic = basics.get(i);
            Integer deluxe = deluxes.get(i);
            Integer total = totals.get(i);

            if (basic * BASIC_CUPCCAKE_COST + deluxe * DELUXE_CUPCCAKE_COST != total) {
                throw new IllegalStateException("Sales and total revenue don't match for " + date);
            }

            ADaysWork aDaysWork = new ADaysWork(date, basic, deluxe, total);
            data.put(date, aDaysWork);
        }

        return data;
    }
}
