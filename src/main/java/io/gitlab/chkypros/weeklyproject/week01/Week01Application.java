package io.gitlab.chkypros.weeklyproject.week01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.IsoFields;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SpringBootApplication
public class Week01Application {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Week01Application.class, args);

        if (args.length != 3) {
            throw new IllegalArgumentException("Invalid Input! Was expecting 3 filepaths: <path for basic> <path for deluxe> <path for totals>");
        }

        InputParser parser = context.getBean(InputParser.class);
        try {
            Map<LocalDate, ADaysWork> data = parser.parse(args[0], args[1], args[2]);

            calculateYearlyRevenueTotals(data);
            System.out.println();

            calculateMonthlyRevenueTotals(data);
            System.out.println();

            calculateWeeklyRevenueTotals(data);
        } catch (IOException e) {
            System.err.println("Failed to open provided files");
            System.exit(1);
        }
    }

    private static void calculateYearlyRevenueTotals(Map<LocalDate, ADaysWork> data) {
        Map<Integer, Integer> yearlyRevenue = data.entrySet().stream().collect(Collectors.groupingBy(
            (Map.Entry<LocalDate, ADaysWork> e) -> e.getKey().getYear(),
            Collectors.summingInt((Map.Entry<LocalDate, ADaysWork> e) -> e.getValue().getTotal())));

        System.out.println("Yearly revenue Totals:");
        yearlyRevenue.entrySet().stream()
            .sorted(Comparator.comparingInt(Map.Entry::getKey))
            .forEach(e -> System.out.println(e.getKey() + " $" + e.getValue()));
    }

    private static void calculateMonthlyRevenueTotals(Map<LocalDate, ADaysWork> data) {
        Map<Integer, Integer> yearlyRevenue = data.entrySet().stream().collect(Collectors.groupingBy(
            (Map.Entry<LocalDate, ADaysWork> e) -> e.getKey().getMonthValue(),
            Collectors.summingInt((Map.Entry<LocalDate, ADaysWork> e) -> e.getValue().getTotal())));

        System.out.println("Monthly revenue Totals:");
        yearlyRevenue.entrySet().stream()
            .sorted(Comparator.comparingInt(Map.Entry::getKey))
            .forEach(e -> System.out.println(e.getKey() + " $" + e.getValue()));
    }

    private static void calculateWeeklyRevenueTotals(Map<LocalDate, ADaysWork> data) {
        Map<Integer, Integer> yearlyRevenue = data.entrySet().stream().collect(Collectors.groupingBy(
            (Map.Entry<LocalDate, ADaysWork> e) -> e.getKey().get(IsoFields.WEEK_OF_WEEK_BASED_YEAR),
            Collectors.summingInt((Map.Entry<LocalDate, ADaysWork> e) -> e.getValue().getTotal())));

        System.out.println("Weekly revenue Totals:");
        yearlyRevenue.entrySet().stream()
            .sorted(Comparator.comparingInt(Map.Entry::getKey))
            .forEach(e -> System.out.println(e.getKey() + " $" + e.getValue()));
    }
}
