package io.gitlab.chkypros.weeklyproject.week01;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
class ADaysWork {
    LocalDate date;
    int basic;
    int deluxe;
    int total;
}
